﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Sinks.Datadog.Logs;
using System;

[assembly: FunctionsStartup(typeof(DealerIntegrationDW.FunctionApp.Startup))]

namespace DealerIntegrationDW.FunctionApp
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddLogging(c =>
            {
                c.AddSerilog(new LoggerConfiguration()
                            .Enrich.FromLogContext()
                            .WriteTo.DatadogLogs(Environment.GetEnvironmentVariable("DD_API_KEY"), configuration: new DatadogConfiguration() { Url = Environment.GetEnvironmentVariable("DD_URL") })
                            .CreateLogger(), true);
            });
        }

        public override void ConfigureAppConfiguration(IFunctionsConfigurationBuilder builder)
        {
            base.ConfigureAppConfiguration(builder);
            builder.ConfigurationBuilder.AddEnvironmentVariables();
        }
    }
}

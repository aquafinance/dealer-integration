using Dapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.Json;
using System.Threading.Tasks;

namespace DealerIntegrationDW.FunctionApp
{
    public sealed class IntegrationFunction
    {
        private readonly IConfiguration configuration;

        public IntegrationFunction(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [FunctionName("ReportQueue_Sub")]
        public async Task Report([ServiceBusTrigger("dealer-integration-and-cop-to-data-warehouse-reporting", Connection = "reportqueue_conn")] string myQueueItem, ILogger log)
        {
            ReportModel model = null;
            try
            {
                model = JsonSerializer.Deserialize<ReportModel>(myQueueItem);
                var connStr = configuration["DW_DB_ReportConnectionString"];
                using var connection = new SqlConnection(connStr);
                var p = new DynamicParameters();
                p.Add("@RequestDataID", model.RequestDataId);
                p.Add("@IntegrationApplicationID", model.IntegrationApplicationId);
                p.Add("@IntegrationID", model.IntegrationId);
                p.Add("@IntegrationName", model.IntegrationName);
                p.Add("@BusinessStatusCode", model.BusinessStatusCode);
                p.Add("@BusinessStatusName", model.BusinessStatusName);
                p.Add("@RequestCreatedBy", model.RequestCreatedBy);
                p.Add("@RequestCreatedDate", model.RequestCreatedDate);
                p.Add("@RequestModifiedBy", model.RequestModifiedBy);
                p.Add("@RequestModifiedDate", model.RequestModifiedDate);
                p.Add("@ApplicationID", model.ApplicationId);
                p.Add("@IsValid", model.IsValid);
                p.Add("@Errors", model.Errors);
                p.Add("@DealerNo", model.LenderDealerId);
                p.Add("@AffiliateId", model.AffiliateId);
                p.Add("@DealerAffiliateName", model.DealerAffiliateName);
                p.Add("@ResponseDataID", model.ResponseDataId);
                p.Add("@FCAApplicationID", model.FcaapplicationId);
                p.Add("@ResponseCreatedBy", model.ResponseCreatedBy);
                p.Add("@ResponseCreatedDate", model.ResponseCreatedDate);
                p.Add("@ResponseModifiedBy", model.ResponseModifiedBy);
                p.Add("@ResponseModifiedDate", model.ResponseModifiedDate);
                p.Add("@AdvantageErrorLogID", model.AdvantageErrorLogId);
                p.Add("@LastAdvantageErrorMsg", model.LastAdvantageErrorMsg);

                await connection.ExecuteAsync("dbo.Dlrtrck_Fx_Parse", p, commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                log.LogError(ex, "Error while processing RequestDataId {requestDataId}: {err}. Model: {model} ", model?.RequestDataId, ex.Message, myQueueItem);
                throw;
            }
        }

        //[FunctionName("DeadQueue")]
        //public async Task ReportQueue_Dead([ServiceBusTrigger("dealer-integration-and-cop-to-data-warehouse-error", Connection = "deadqueue_conn")] string queueItem, ILogger log)
        //{
        //    try
        //    {
        //        var model = JsonSerializer.Deserialize<ErrorModel>(queueItem);
        //        var connStr = configuration["DW_DB_ReportConnectionString"];
        //        using var connection = new SqlConnection(connStr);
        //        var p = new DynamicParameters();
        //        p.Add("@QueueName", model.QueueName);
        //        p.Add("@MsgError", model.ErrorMsg);
        //        await connection.ExecuteAsync("dbo.Dlrtrck_Fx_BadMsg", p, commandType: CommandType.StoredProcedure);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.LogError(ex, "Error while processing report model: {err} ", ex.Message);
        //    }
        //}
    }


    public class ReportModel
    {
        public int RequestDataId { get; set; }
        public string IntegrationApplicationId { get; set; }
        public int IntegrationId { get; set; }
        public string IntegrationName { get; set; }
        public string BusinessStatusCode { get; set; }
        public string BusinessStatusName { get; set; }
        public string RequestCreatedBy { get; set; }
        public DateTime? RequestCreatedDate { get; set; }
        public string RequestModifiedBy { get; set; }
        public DateTime? RequestModifiedDate { get; set; }
        public int ApplicationId { get; set; }
        public bool? IsValid { get; set; }
        public string Errors { get; set; }
        public string LenderDealerId { get; set; }
        public string AffiliateId { get; set; }
        public string DealerAffiliateName { get; set; }
        public int? ResponseDataId { get; set; }
        public string FcaapplicationId { get; set; }
        public string ResponseCreatedBy { get; set; }
        public DateTime? ResponseCreatedDate { get; set; }
        public string ResponseModifiedBy { get; set; }
        public DateTime? ResponseModifiedDate { get; set; }
        public int? AdvantageErrorLogId { get; set; }
        public string LastAdvantageErrorMsg { get; set; }
    }

    public class ErrorModel
    {
        public string QueueName { get; set; }
        public string ErrorMsg { get; set; }
    }
}

